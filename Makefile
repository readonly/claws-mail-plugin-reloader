NAME = plugin-reloader
LIB = $(NAME).so
OBJ = $(NAME).o

PREFIX ?= /usr/local
PLUGINS_DIR ?= $(PREFIX)/lib/claws-mail/plugins

CFLAGS += `pkg-config --cflags gtk+-2.0 claws-mail` -fPIC -g \
		  -I$(PREFIX)/include/claws-mail/common
LDFLAGS += `pkg-config --libs gtk+-2.0 claws-mail`

$(LIB): $(OBJ)
	$(CC) $(LDFLAGS) -shared $^ -o $@

$(DESTDIR)$(PLUGINS_DIR):
	mkdir -p $@

install: $(LIB) | $(DESTDIR)$(PLUGINS_DIR)
	cp $< $|

uninstall:
	rm $(DESTDIR)$(PLUGINS_DIR)/$(LIB)

clean:
	rm -f *.o $(LIB)

.PHONY: clean install uninstall
